
Pod::Spec.new do |s|

  s.name         = "Konotor"
  s.version      = "0.0.7"
  s.summary      = "A Konotor ios sdk pod spec file."
  s.description  = "A Konotor ios sdk pod spec file for intergarating konotor push notifications."
  s.homepage     = "https://www.innoventestech.com"
  s.license      = { :type => "MIT", :file => "LICENSE.txt" }
  s.author             = { "Sukanya Raj" => "sukanya@innoventestech.com" }
  s.platform     = :ios, "5.1"
  s.source       = { :git => "https://Sukanya_raj@bitbucket.org/Sukanya_raj/konotor-ios-sdk.git", :tag => "0.0.7" }
  s.source_files  = "Konotor/*/*.{h,m}","Konotor/*/*/*.h"
  s.preserve_paths = "Konotor/include/Konotor/*.h","Konotor/libKonotorCombined.a", "Konotor/KonotorModels.bundle"
  s.resources = "Konotor/*/*/*.png", "Konotor/KonotorModels.bundle", "Konotor/*/*.xib"
  s.ios.vendored_library = "Konotor/libKonotorCombined.a"
  s.frameworks = "Foundation", "UIKit", "AVFoundation", "CoreGraphics", "AudioToolbox", "CoreMedia", "CoreData", "ImageIO", "QuartzCore"
  s.xcconfig       = { 'LIBRARY_SEARCH_PATHS' => '"$(PODS_ROOT)/Konotor"' }
  s.requires_arc = true

end
